A library for Dart developers.

## Usage

A simple usage example will be here someday...

```
.yaml options

targets:
  $default:
    builders:
      helper_anotations|enum_like:
        options:
          dollar_prefix: true|false
	  only_const: true|false
```

## Features and bugs

Please file feature requests and bugs at the [dev/null].

