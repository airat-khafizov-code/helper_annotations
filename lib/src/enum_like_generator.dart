import 'package:analyzer/dart/element/element.dart';
import 'package:build/build.dart';
import 'package:source_gen/source_gen.dart';

import 'package:helper_annotations/annotations.dart';
import 'package:helpers/helpers.dart';

class EnumLikeGenerator extends GeneratorForAnnotation<EnumLike> {
  BuilderOptions options;

  EnumLikeGenerator(this.options);

  @override
  String generateForAnnotatedElement(
      Element element, ConstantReader annotation, BuildStep buildStep) {
    if (!(element is ClassElement)) {
      throw InvalidGenerationSourceError(
          '@EnumLike annotation should be used on classes only!',
          element: element);
    }
    bool dollarPrefix = options.config['dollar_prefix'] is bool
        ? options.config['dollar_prefix']
        : false;
    bool onlyConst = options.config['only_const'] is bool
        ? options.config['dollar_prefix']
        : false;

    //Looking for static const class exemplars
    var staticMembers =
        element.fields.fold<Set<FieldElement>>({}, (previous, field) {
      if (field.isStatic &&
          (onlyConst ? field.isConst : true) &&
          field.type.toString() == element.name) {
        previous.add(field);
      }
      return previous;
    });
    final classType = element.name;
    final asExtension = annotation.read('asExtension').boolValue;
    final nonPrivate = staticMembers.where((element) => !element.isPrivate);
    var mapName = '_${classType}EnumMap'.toLowerCaseFirst();
    var valuesName = '_${classType}Values'.toLowerCaseFirst();
    var nameName = '_${classType}Name'.toLowerCaseFirst();
    var byNameName = '_${classType}ByName'.toLowerCaseFirst();
    if (dollarPrefix) {
      mapName = mapName.replaceFirst('_', '_\$');
      valuesName = valuesName.replaceFirst('_', '_\$');
      nameName = nameName.replaceFirst('_', '_\$');
      byNameName = byNameName.replaceFirst('_', '_\$');
    }
    final buff = StringBuffer();

    //Generating private functions
    buff.write(
        '///[$mapName] - Map for static const $classType public members, [key] - variable name, [value] - variable\n');
    buff.write(_createNameMap(classType, nonPrivate, mapName));
    buff.write(
        '///[$valuesName] - Iterable, contains all static const $classType public members in declaration order\n');
    buff.write(_createValues(classType, mapName, valuesName));
    buff.write(
        '///[$byNameName] - Property returns $classType variable by variable name, or empty String if there is no such variable\n');
    buff.write(_createGetByName(classType, mapName, byNameName));
    buff.write('///[$nameName] - returns variable name of this $classType\n');
    buff.write(_createName(classType, mapName, nameName));

    if (asExtension) {
      //creating extension on our class
      buff.write('''
    ///GENERATED EXTENSION WRAPPER BY @EnumLike:
    ///Added [enumMap], [values], [byName] and [name] properties  
    ''');
      buff.write(
          'extension ${dollarPrefix ? '\$' : ''}${classType}GeneratedExtension on $classType {');
      buff.write(
          '///[\$enumMap] - Map for static const $classType public members, [key] - variable name, [value] - variable\n');
      buff.write(
          'static Map<String, $classType> get ${mapName.replaceFirst('_', '')} => $mapName;');
      buff.write(
          '///[\$values] - Iterable, contains all static const $classType public members in declaration order\n');
      buff.write(
          'static Iterable<$classType> get ${valuesName.replaceFirst('_', '')} => $valuesName;');
      buff.write(
          '///[\$byName] - Property returns $classType variable by variable name, or empty String if there is no such variable\n');
      buff.write(
          'static $classType? ${byNameName.replaceFirst('_', '')}(String name) => $byNameName(name);');
      buff.write('///[name] - returns variable name of this\n');
      buff.write(
          'String get ${nameName.replaceFirst('_', '')} => $nameName(this);');
      buff.write('}');
    }
    return buff.toString();
  }

  String _createNameMap(
          String classType, Iterable<FieldElement> values, String mapName) =>
      'Map<String, $classType> $mapName = Map<String, $classType>'
      '.unmodifiable({${values.fold<String>('', (previousValue, element) => //
          "$previousValue, '${element.name}' : $classType.${element.name}").replaceFirst(', ', '')}});';

  String _createValues(String classType, String mapName, String funcName) =>
      'Iterable<$classType> get $funcName => $mapName.values;';

  String _createGetByName(String classType, String mapName, String funcName) =>
      '$classType? $funcName(String name) => $mapName.containsKey(name) ? $mapName[name] : null;';

  String _createName(String classType, String mapName, String funcName) =>
      "String $funcName($classType enumLike) => $mapName.keys.firstWhere((key) => $mapName[key] == enumLike, orElse: () => '');";
}
