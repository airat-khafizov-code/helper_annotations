library helper_annotations.builder;

import 'package:build/build.dart';
import 'package:helper_annotations/helper_annotations.dart';
import 'package:source_gen/source_gen.dart';

Builder enumLikeBuilder(BuilderOptions options) =>
    SharedPartBuilder([EnumLikeGenerator(options)], 'enumlike');
